# Grid Automated #

Grid automated is a script created to generate the selenium grid's browsers instances on demand. The idea behind this automation is to make the instance generation dynamic i.e. making the browser instance available as and when requested by the script.

## Summary ##

* Grid automated is a script created to generate the selenium grid's browsers instances on demand. The user simply needs to call a function '**createBrowserInstance**', and the script will automatically start the selenium server return you the browser instance configured based on the [DesiredCapabilities](https://code.google.com/p/selenium/wiki/DesiredCapabilities).

## Quick Start ##

* Prerequisites - Install Node, Java (SE Development Kit).

* Download the project, navigate to the root folder using command prompt, and run npm -install. This should install all the dependencies ('q', and 'request' as of now).

* Include the file reference **'./selenium-manager/driver-seleniumInstance-binder'**.

* Call the function **createBrowserInstance**.

* Calling function **createBrowserInstance** will return a JavaScript object with the details of the instance started in the back-end. 

###Sample Code###

```
var gridAutomated = require('./selenium-manager/driver-seleniumInstance-binder');
var myEdgeInstance = gridAutomated.createBrowserInstance('edge');
console.log(myEdgeInstance);
```
###Output###
```
{ seleniumProcess: { name: 'selenium', port: 4444, pid: 3716, status: 'active' },
  process:
   { name: 'Microsoft-Edge',
     port: 5000,
     pid: 4032,
     status: 'Active' },
  options:
   { host: 'xx.xxx.xxx.xxx',
     port: 5000,
     desiredCapabilities:
      { browserName: 'MicrosoftEdge',
        takesScreenshot: true,
        javascriptEnabled: true,
        acceptSslCerts: true } } }
```

## Updating Desired Capabilities ##

* In order to **update the desired capabilities**, the user needs to update the **driver-configuration.js** file placed in the folder **selenium-manager**. You need not update the host, and port configuration in this file, as these values will be generated on the run time.

###Snippet for Phantom ###
```
driverConfig.Phantom = {
    host: '',
    port: 0,
    desiredCapabilities : {
        browserName : 'phantomjs',
        'phantomjs.binary.path' : '../selenium-executables/drivers/phantomjs.exe',
	    takesScreenshot	: true,
        javascriptEnabled : true,
        acceptSslCerts : true
        //Add more capabilities here ...
    }
};
```

###Snippet for Internet Explorer###
```
driverConfig.Edge = {
    host: '',
    port: 0,
    desiredCapabilities : {
        browserName : 'MicrosoftEdge',
		takesScreenshot	: true,
        javascriptEnabled : true,
        acceptSslCerts : true

    }
};
```

## Who do I talk to? ##

* Jatin Sethi @ jatinsethi@live.in